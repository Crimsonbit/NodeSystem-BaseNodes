package at.crimsonbit.nodesystem.node.base;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

/**
 * 
 * @author Florian Wagner
 *
 */
public enum Base implements INodeType {

	OUTPUT("Output Node"), INFO("");

	private String name;

	private Base(String s) {

		this.name = s;
	}

	@Override
	public String regName() {
		return this.name;
	}

}
