package at.crimsonbit.nodesystem.node.path;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class PathNode extends AbstractNode {

	@NodeField
	@NodeOutput("compute")
	Object path;

	@Override
	public void compute() {
	}

}
