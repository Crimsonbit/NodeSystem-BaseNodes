package at.crimsonbit.nodesystem.node.constant;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class IntegerConstantNode extends AbstractNode {

	@NodeField
	@NodeOutput("compute")
	int constant;

	@Override
	public void compute() {
	}

}
