package at.crimsonbit.nodesystem.node.module;

import at.crimsonbit.nodesystem.node.constant.BooleanConstantNode;
import at.crimsonbit.nodesystem.node.constant.ByteConstantNode;
import at.crimsonbit.nodesystem.node.constant.CharConstantNode;
import at.crimsonbit.nodesystem.node.constant.ConstantNodes;
import at.crimsonbit.nodesystem.node.constant.DoubleConstantNode;
import at.crimsonbit.nodesystem.node.constant.FloatConstantNode;
import at.crimsonbit.nodesystem.node.constant.IntegerConstantNode;
import at.crimsonbit.nodesystem.node.constant.LongConstantNode;
import at.crimsonbit.nodesystem.node.constant.ShortConstantNode;
import at.crimsonbit.nodesystem.node.constant.StringConstantNode;
import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;

public class BaseNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		// registry.registerDefaultFactory(Base.OUTPUT, OutputNode.class);

		registry.registerDefaultFactory(ConstantNodes.BOOLEAN_CONSTANT, BooleanConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.BYTE_CONSTANT, ByteConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.CHAR_CONSTANT, CharConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.DOUBLE_CONSTANT, DoubleConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.FLOAT_CONSTANT, FloatConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.INTEGER_CONSTANT, IntegerConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.LONG_CONSTANT, LongConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.SHORT_CONSTANT, ShortConstantNode.class);
		registry.registerDefaultFactory(ConstantNodes.STRING_CONSTANT, StringConstantNode.class);

		// registry.registerDefaultFactory(Path.PATH, PathNode.class);
	}

}
